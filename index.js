const Koa = require("koa");
const serve = require("koa-static");
const koaBody = require("koa-body");
const Router = require("koa-router");
const { glob } = require("glob");
const path = require("path");
const niv = require("node-input-validator");
const cors = require("@koa/cors");
const logger = require("./src/lib/logger");

// intializing DB
require("./src/config/db-config");

// Intializing application
const app = new Koa();

// Intializing Router
const router = new Router();

// Body-parser
app.use(
  koaBody({
    multipart: true,
    formidable: {
      keepExtensions: true,
    },
  })
);

// Error handling
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (error) {
    let status = 500;
    let message = "Something went wrong";

    if (error.status) {
      if (error.status >= 500) {
        // eslint-disable-next-line no-multi-assign
        status = ctx.status = 500;
      }
      // eslint-disable-next-line no-multi-assign
      status = ctx.status = error.status;
    }

    if (error.message) {
      message = error.message;
    }

    ctx.body = {
      status,
      message,
    };

    logger.error("ERROR::: ", error);

    ctx.app.emit("error", error, ctx);
  }
});

router.get("/api/v1/erp", async (ctx) => {
  ctx.body = { message: "welcome to the erp APIs" };
});

// setting up input validator
app.use(niv.koa());

// serving static files
app.use(serve(__dirname + "/uploads"));

// Creating routes
glob(path.resolve("src/modules/**/*.routes.js"), (err, files) => {
  if (err) throw err;
  files.forEach((file) => {
    let routes = require(`${file}`);
    if (routes instanceof Router) {
      router.use("/api/v1/erp", routes.routes(), routes.allowedMethods());
    }
  });

  logger.info("Routes created");
  app.use(router.routes()).use(router.allowedMethods({ throw: true }));
});

// enabling cors
app.use(cors());

const port = process.env.PORT || 8080;

app.listen(port, () => logger.info(`Server is listening on port::${port}`));

module.exports = app;
