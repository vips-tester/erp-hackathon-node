const mongoose = require('mongoose');
const logger = require('../lib/logger');

// Connect monogo db
module.exports = mongoose.connect(`mongodb+srv://admin:admin@cluster0.womz332.mongodb.net/?retryWrites=true&w=majority`, {
  //  user: process.env.DB_UserName,
  //  pass: process.env.DB_Password,

  
  dbName: process.env.DB_NAME,
  maxPoolSize: 10,
  connectTimeoutMS: 10000,
  useNewUrlParser: true,
  useUnifiedTopology: true,

}).then(() => {
  logger.info('db connected');
}).catch((err) => {
  logger.error(`Error in db connectivity:: ${err}`);
});
