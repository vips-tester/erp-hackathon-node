const winston = require('winston');
require('winston-daily-rotate-file');

const logger = winston.createLogger({
  level: ['info'],
  format: winston.format.simple(),
  defaultMeta: { service: 'user-service' },
  transports: [
    new winston.transports.File({ filename: 'logs/error.log', level: 'error' }),
    new winston.transports.File({ filename: 'logs/combined.log' }),
    new winston.transports.DailyRotateFile({
      dirname: './logs/rotator/',
      filename: 'team-x%DATE%.log',
      datePattern: 'YYYY-MM-DD-HH',
      maxFiles: '1d',
      //maxSize: '10m',
    }),
  ],
});

if (process.env.ENV !== 'production') {
  logger.info('Im here');
  logger.add(new winston.transports.Console({
    format: winston.format.simple(),
  }));
}

module.exports = logger;
