'use strict';
const nodemailer = require('nodemailer'),
  fs = require('fs'),
  ejs = require('ejs'),
  path = require('path');

require('dotenv').config();

const moment = require('moment');

class Mail {
  async render(which, data) {
    // set system default properties
    data.$moment = moment;
    data.$appName = process.env.APP_NAME;

    let filename = path.resolve(process.cwd(), 'src/resources/emails', which.replace('.', '/') + '.ejs.html');
    return await new Promise((resolve, reject) => {
      ejs.renderFile(filename, data, function (err, data) {
        if (err) return reject(err);
        resolve(data);
      });
    });
  }

  async template(to, subject, which, data = {}) {
    try {
      data.subject = subject;
      const html = await this.render(which, data);

      // setup email data with unicode symbols
      let mailOptions = {
        to: to, // list of receivers
        subject: subject,
        html: html,
        replyTo: data.email
      };
      this.send(mailOptions, data);

      return true;

    } catch (e) {
      console.error(e);
    }
    return false;
  }

  async send(mailOptions, data) {
    return await this.transporter(mailOptions, data);
  }

  async transporter(options, data) {

    return new Promise((resolve, reject) => {

      // Generate test SMTP service account from ethereal.email
      // Only needed if you don't have a real mail account for testing

      // create reusable transporter object using the default SMTP transport
      let transporter = nodemailer.createTransport({
        //@ts-ignore
        host: process.env.MAIL_HOST || "smtp.gmail.com",
        port: process.env.MAIL_PORT || 465,
        secure: process.env.MAIL_SECURE || true, // true for 465, false for other ports
        auth: {
          user: process.env.MAIL_USER, // generated ethereal user
          pass: process.env.MAIL_PASSWORD// generated ethereal password
        }
      });

      options.from = `${process.env.APP_NAME} <${process.env.MAIL_FROM}>`

      // send mail with defined transport object
      transporter.sendMail(options, (error, info) => {
        if (error) {
          reject(error);
          return console.error(error);
        }

        resolve(info);
        console.log('Message sent: %s', info.messageId);

      });
    }).catch((e) => {
      console.error(e);
    });
  }
}

module.exports = new Mail