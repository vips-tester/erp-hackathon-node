const fs = require('fs');
const path = require('path');
const bcrypt = require("bcrypt");


function generateOtp() {

    var digits = '0123456789';
    let otp = '';
    for (let i = 0; i < 6; i++) {
        otp += digits[Math.floor(Math.random() * 10)];
    }
    return otp;
}

async function uploadFile(ctx) {

    const dir = path.join(__dirname, '../', '/uploads');

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    const filename = path.basename(ctx.request.files.image.path);


    // Read the file
    fs.readFile(ctx.request.files.image.path, function (err, data) {
        if (err) throw err;
        console.log('File read!');

        // Write the file
        fs.writeFile(dir + '/' + filename, data, function (err) {
            if (err) throw err;
            // res.write('File uploaded and moved!');
            // res.end();
            console.log('File written!');
        });

        // Delete the file
        fs.unlink(ctx.request.files.image.path, function (err) {
            if (err) throw err;
            console.log('File deleted!');
        });
    });




    // fs.renameSync(ctx.request.files.image.path, dir + '/' + filename);

    return filename;

}

async function encryptPassword(rawPassword) {
    const salt = await bcrypt.genSalt(+process.env.SALT_VALUE);
    const password = await bcrypt.hash(rawPassword, salt);
    return password;
}

function generatePassword() {
    var length = 6,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
        retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
        retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
}




module.exports = { generateOtp, uploadFile, encryptPassword, generatePassword };