const jwt = require('jsonwebtoken');
const user = require('../models/user');

function verifyToken(ctx) {
  const rawToken = ctx.headers['x-token'];
  if (!rawToken) {
    ctx.throw(401, 'No token provided');
  }

  let decodedToken;

  try {
    decodedToken = jwt.verify(rawToken, process.env.SECRETKEY);
  } catch (error) {
    ctx.throw(401, 'Invalid token');
  }

  return [rawToken, decodedToken];
}

async function userInit(ctx, next) {
  const [rawToken, decodedToken] = verifyToken(ctx);
  const query = {
    email: decodedToken.email,
    // role: decodedToken.role,
    //phone: decodedToken.phone
  };

  const model = await user.findOne(query);
  if (!model) {
    ctx.throw(404, 'unauthorized user');
  }

  ctx.user = model;

  return next();
}

module.exports = { userInit };
