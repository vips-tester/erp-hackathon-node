const mongoose = require('mongoose');

const schema = new mongoose.Schema({

  inTime: {
    type: String,
    //required: true,
  },
  outTime: {
    type: String,
    
  },

  status: {
    type: String,
    default:0
  },

  location: String,

  created_by: {
    type: mongoose.SchemaTypes.ObjectId,
    ref: 'User'
  },
  user_id: [{
    type: mongoose.SchemaTypes.ObjectId,
    ref: 'User'
  }]
}, {
  timestamps: true
});


module.exports = mongoose.model('clockinOut', schema);
