const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  expenseAmount: {
    type: Number,
    //required: true,
  },
  category: {
    type: String,
  },

  comment: {
    type: String,
  },

  split_by: {
    type: String,
  },

  email: {
    type: String,
  },

  description: {
    type: String,
  },

  team_id: {
    type: mongoose.SchemaTypes.ObjectId,
    ref: 'team'
  },

  user_id: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  assignExpense: {
    type: Number,
  },

  addedAt: {
    type: String,
  },

}, {
  timestamps: true
});

module.exports = mongoose.model('Expense', schema);
