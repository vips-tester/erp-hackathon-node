const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  firstName:String,
  middleName:String,
  lastName:String,
  gender:String,
  dob:String,
  maritalStatus:String,
  bloodGroup:String,
  manager:String,
  
  workEmail:String,
  personalEmail:String,
  mobile:String,
  residencePhone:String,
  workPhone: Number,
  Skype:String,

  degree:String,
  branch:String,
  start:String,
  end:String,
  percentage: Number,
  school: String,

  organization: String,
  designation: String,
  org_start: String,
  org_end: String,
  location: String,
  password: String,  


},{
  timestamps:true
});

module.exports = mongoose.model('User', schema);
