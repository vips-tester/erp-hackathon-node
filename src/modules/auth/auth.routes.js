const Router = require("koa-router");
const otp = require("./controller/auth.controller.otp");
const signup = require("./controller/auth.controller.signup");
const forgetPassword = require("./controller/auth.controller.forgetPassword");
const login = require("./controller/auth.controller.login");
const resetPassword = require("./controller/auth.controller.resetPassword");
const addSuggestion = require("./controller/auth.controller.suggestion");

const clockinOut = require("./controller/auth.controller.clockinOut.js");
const findUser = require("./controller/auth.controller.findUser.js");
const findAllUser = require("./controller/auth.controller.findAll.js");

const route = new Router({
  prefix: "/auth",
});

route.post("/generate/otp", otp.getOtp);
route.post("/signup", signup);
route.post("/forgetPassword", forgetPassword);
route.post("/login", login);
route.post("/resetPassword", resetPassword);
route.post("/addSuggestion", addSuggestion);
route.post("/clockinOut", clockinOut);
route.post("/findUser", findUser);
route.get("/findAllUser", findAllUser);

module.exports = route;
