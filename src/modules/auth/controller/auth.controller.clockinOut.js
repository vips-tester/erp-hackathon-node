      const jwt = require('jsonwebtoken');
      const { user, clockinOut } = require('../../../models');
      const otpController = require('./auth.controller.otp');
      const bcrypt = require("bcrypt");
      const { encryptPassword } = require('../../../lib/util');
      const { log } = require('winston');

      async function userClockInOrOut(ctx) {
      // if (!await (otpController.verifyOtp(ctx))) {
      //   ctx.throw(400, 'Invalid code');
      // }

      const { body } = ctx.request;

      const model = await user.findById(body.user_id);
      if (!model) {
      ctx.throw(404, 'user not found');
      }

      const createUser = await clockinOut.create({
      inTime: body.inTime,
      outTime: body.outTime,
      status: body.status,
      location: body.location,
      user_id: body.user_id,

      });

      const message = body.inTime ? 'clock in' : 'clock out'

      ctx.body = { message, data: createUser };

      }


      module.exports = userClockInOrOut;
