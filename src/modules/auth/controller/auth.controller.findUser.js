const jwt = require('jsonwebtoken');
const { user } = require('../../../models');
const otpController = require('./auth.controller.otp');
const bcrypt = require("bcrypt");
const { encryptPassword } = require('../../../lib/util');
const { log } = require('winston');

async function findUser(ctx) {
  // if (!await (otpController.verifyOtp(ctx))) {
  //   ctx.throw(400, 'Invalid code');
  // }

  const { body } = ctx.request;
  
  const model = await user.find({ $or: [{ firstName: new RegExp(body.search, 'ig') },{ lastName: new RegExp(body.search, 'ig') }, { workEmail: new RegExp(body.search, 'ig') }] });

  
  if (!model) {
    ctx.throw(404, 'user not found');
  }


  ctx.body = { message: 'sucess', data: model };

}


module.exports = findUser;
