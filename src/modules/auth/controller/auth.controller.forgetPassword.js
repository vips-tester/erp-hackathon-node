const jwt = require('jsonwebtoken');
const { user } = require('../../../models');
const otpController = require('./auth.controller.otp');
const bcrypt = require("bcrypt");
const { generatePassword , encryptPassword } = require('../../../lib/util');

const Mail = require('../../../lib/mail');

const autoGenerate = generatePassword();

const expirationTime = Date.now() + 1000 * 60 * 3; // expiration time is 3 minutes


async function forgetPassword(ctx) {
   
  const { body } = ctx.request;
  const { value } = {};
  const model = await user.findOne({ email: body.email });
  if (!model) {
    ctx.throw(404, 'user not found');
  }

  model.expired_at = expirationTime;
  model.is_temporary = true;
  model.password = await encryptPassword(autoGenerate);
    
  await model.save();
  ctx.body = { message: 'Password send on mail' }; 
  sendMail(autoGenerate, body.email);

}


function sendMail(generatePassword , email) {
  Mail.template(
    email,
    'Forgot password',
    'generatePassword',
    {
      email,
      generatePassword,
      autoGenerate,
    },
  );
}

module.exports = forgetPassword;
