const jwt = require('jsonwebtoken');
const { user } = require('../../../models');
const otpController = require('./auth.controller.otp');
const bcrypt = require("bcrypt");
const { encryptPassword } = require('../../../lib/util');
const { log } = require('winston');

async function login(ctx) {
  // if (!await (otpController.verifyOtp(ctx))) {
  //   ctx.throw(400, 'Invalid code');
  // }

  const { body } = ctx.request;

  const model = await user.findOne({ workEmail: body.workEmail });
  if (!model) {
    ctx.throw(404, 'user not found');
  }


  const validPassword = await bcrypt.compare(body.password, model.password);
  console.log('valid pass:::: ', validPassword)
  if (!validPassword) {
    ctx.throw(400, 'password incorrect');
  }

  model.password = undefined;

  const token = jwt.sign(model.toObject(), process.env.SECRETKEY, { expiresIn: '365d' });

  ctx.body = { message: 'user login', token: token, data: model};

}


module.exports = login;
