const { generateOtp } = require('../../../lib/util');
const { otp, user } = require('../../../models');
const Mail = require('../../../lib/mail');

async function getOtp(ctx) {
  await ctx.validate({
    email: 'required|email',
  });

  const generatedOTP = generateOtp();

  const expirationTime = Date.now() + 1000 * 60 * 3; // expiration time is 3 minutes

  await otp.create({
    otp: generatedOTP,
    expired_at: expirationTime,
  });

  await user.updateOne({
    email: ctx.request.body.email,
  }, { $set: { email: ctx.request.body.email, role: ctx.request.body.role } }, { upsert: true });

  // eslint-disable-next-line no-use-before-define
  sendOtp(generatedOTP, ctx.request.body.email);

  ctx.body = { status: 200, data: generatedOTP };
}

async function verifyOtp(ctx) {
  const { code } = ctx.request.body;

  const model = await otp.findOne({ otp: code });

  if (!model || model.expired_at < Date.now()) {
    return false;
  }

  model.is_verified = true;
  model.save();

  return true;
}

// eslint-disable-next-line no-shadow
function sendOtp(otp, email) {
  Mail.template(
    email,
    'Login otp',
    'otp',
    {
      email,
      otp,
    },
  );
}

module.exports = { getOtp, verifyOtp };
