const jwt = require('jsonwebtoken');
const { user } = require('../../../models');
const otpController = require('./auth.controller.otp');
const bcrypt = require("bcrypt");
const { generatePassword, encryptPassword } = require('../../../lib/util');
const Mail = require('../../../lib/mail');
const autoGenerate = generatePassword();



async function resetPassword(ctx) {

  const { body } = ctx.request;

  const model = await user.findOne({ email: body.email });
  if (!model) {
    ctx.throw(404, 'user not found');
  }

  const validPassword = await bcrypt.compare(body.pre_password, model.password);
  if (!validPassword) {
    ctx.throw(400, 'password incorrect');
  }

  //const password = await encryptPassword(body.new_password);

  // if (model.expired_at < Date.now()) {
  //   ctx.throw(400, 'password expired');
  // }

  model.password = await encryptPassword(body.new_password);
  await model.save();
  ctx.body = { message: 'password updated' };
}

module.exports = resetPassword;
