const jwt = require('jsonwebtoken');
const { user } = require('../../../models');
const otpController = require('./auth.controller.otp');
const bcrypt = require("bcrypt");
const { encryptPassword } = require('../../../lib/util');

async function signup(ctx) {
  const { body } = ctx.request;

  const model = await user.findOne({ workEmail: body.workEmail });
  if (model) {
    ctx.throw(404, 'User alresdy exist');
  }
  const password = await encryptPassword(body.password);

  const createUser = await user.create({
    password: password,
    workEmail: body.workEmail,

    firstName:body.firstName,
    middleName:body.middleName,
    lastName:body.lastName,
    gender:body.gender,
    dob:body.dob,
    maritalStatus:body.maritalStatus,
    bloodGroup:body.bloodGroup,
    manager:body.manager,
    
    workEmail:body.workEmail,
    personalEmail:body.personalEmail,
    mobile:body.mobile,
    residencePhone:body.residencePhone,
    workPhone: body.workPhone,
    Skype:body.Skype,
  
    degree:body.degree,
    branch:body.branch,
    start:body.start,
    end:body.end,
    percentage: body.percentage,
    school: body.school,
  
    organization: body.organization,
    designation: body.designation,
    org_start: body.org_start,
    org_end: body.org_end,
    location: body.location,

  });

  createUser.password = undefined;
  const token = jwt.sign(createUser.toObject(), process.env.SECRETKEY, { expiresIn: '365d' });
  ctx.body = { message: 'user registered', token };
}


module.exports = signup;
