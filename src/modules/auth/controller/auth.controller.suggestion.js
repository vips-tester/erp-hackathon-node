const jwt = require('jsonwebtoken');
const { suggestion } = require('../../../models');

async function addSuggestion(ctx) {
  const { body } = ctx.request;

  const createUser = await suggestion.create({
    user_id: body.user_id,
    suggestion: body.suggestion
  });


  ctx.body = { message: 'suggestion added'};
}


module.exports = addSuggestion;
