const { uploadFile } = require('../../../lib/util');
const { user } = require('../../../models');

async function updateProfile(ctx) {
  const { body } = ctx.request;

  // eslint-disable-next-line no-underscore-dangle
  const model = await user.findById(ctx.user._id);

  if (ctx.request.files) {
    var upload = await uploadFile(ctx);
  }

  model.email = body.email;
  model.phone = body.phone;
  model.role = body.role;
  model.fullname = body.fullname;
  model.location = body.location;
  model.dob = body.dob;
  model.temp_player = '1';
  model.name = body.name;
  model.image = upload;
  model.upi = body.upi;
  await model.save();

  ctx.body = { message: 'profile updated' };
}

module.exports = updateProfile;
