const Router = require('koa-router');
const { userInit } = require('../../middlewares/authentication_middleware');
const updateProfile = require('./controller/user.controller.update_profile');

const route = new Router({
    prefix: '/user'
});

route.patch('/update/profile', userInit, updateProfile);

module.exports = route;