const moment = require('moment')

var startDate = moment("2022-12-08 09:30:00", "YYYY-MM-DD hh:mm:ss");
var endDate = moment("2022-12-08 18:00:00", "YYYY-MM-DD hh:mm:ss");

var result = `${endDate.diff(startDate, 'hours')}:${endDate.diff(startDate, 'minutes') % 60} `


console.log(result)